
import {defineStore} from 'pinia'
export const userListStore = defineStore('user',{
    state:()=>{
        return {
            count:100
        }
    },
    getters: {
        
    },
    actions: {
        changeCount(payload){
            this.count += payload
        }
    }
})