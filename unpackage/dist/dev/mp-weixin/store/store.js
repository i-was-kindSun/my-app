"use strict";
var common_vendor = require("../common/vendor.js");
const userListStore = common_vendor.defineStore("user", {
  state: () => {
    return {
      count: 100
    };
  },
  getters: {},
  actions: {
    changeCount(payload) {
      this.count += payload;
    }
  }
});
exports.userListStore = userListStore;
