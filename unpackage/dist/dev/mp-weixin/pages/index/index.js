"use strict";
var common_vendor = require("../../common/vendor.js");
const _sfc_main = {
  __name: "index",
  setup(__props) {
    const title = common_vendor.ref("\u6D4B\u8BD5");
    const count = common_vendor.ref(100);
    const changeCount = () => {
      count.value += 100;
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.t(title.value),
        b: common_vendor.t(count.value),
        c: common_vendor.o(changeCount)
      };
    };
  }
};
var MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "C:/Users/\u80E1\u667A\u4F1F/Desktop/2219/myApp/pages/index/index.vue"]]);
wx.createPage(MiniProgramPage);
