"use strict";
var common_vendor = require("../../common/vendor.js");
var api_request = require("../../api/request.js");
var store_store = require("../../store/store.js");
if (!Array) {
  const _component_van_button = common_vendor.resolveComponent("van-button");
  _component_van_button();
}
const _sfc_main = {
  __name: "home",
  setup(__props) {
    const store = store_store.userListStore();
    const count = common_vendor.computed$1(() => store.count);
    const bannerList = common_vendor.ref([]);
    const { changeCount } = store;
    const getBanners = async () => {
      let res = await api_request.request("/banners", "GET");
      console.log(res);
      res.data.result.map((item) => {
        console.log(item);
      });
      bannerList.value = res.data.result;
    };
    return (_ctx, _cache) => {
      return {
        a: common_vendor.o(($event) => getBanners()),
        b: common_vendor.p({
          type: "default"
        }),
        c: common_vendor.o(($event) => common_vendor.unref(changeCount)(10)),
        d: common_vendor.p({
          type: "primary"
        }),
        e: common_vendor.p({
          type: "info"
        }),
        f: common_vendor.p({
          type: "warning"
        }),
        g: common_vendor.p({
          type: "danger"
        }),
        h: common_vendor.t(common_vendor.unref(count)),
        i: common_vendor.f(bannerList.value, (item, index, i0) => {
          return {
            a: index
          };
        })
      };
    };
  }
};
var MiniProgramPage = /* @__PURE__ */ common_vendor._export_sfc(_sfc_main, [["__file", "C:/Users/\u80E1\u667A\u4F1F/Desktop/2219/myApp/pages/home/home.vue"]]);
wx.createPage(MiniProgramPage);
