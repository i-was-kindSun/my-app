export const baseUrl = "http://101.37.23.156:2219/app"

 const request = (url,method="GET",data,header)=>{
    return new Promise((resolve,reject)=>{
        // wx.showLoading({
        //     title:''
        // })
        wx.request({
            url:baseUrl+url,
            method,
            data,
            header,
            success:(res)=>{
                resolve(res)
            },
            fail:(err)=>{
                reject(err)
            }
            
        })
    })
}

export default request